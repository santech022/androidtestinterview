package com.example.sanjeeb.androidinterviewtest.module.patientdetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sanjeeb.androidinterviewtest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This fragment is used for showing patient details. Fragments are being used instead
 * of activity as view to adapt to various screen sizes of android devices(e.g., Tablets)
 * */

public class PatientDetailsFragment extends Fragment {

    @BindView(R.id.tv_patient_lastname) TextView tvPatientLastname;
    @BindView(R.id.tv_patient_firstname) TextView tvPatientFirstname;
    @BindView(R.id.tv_clinician) TextView tvClinician;
    @BindView(R.id.tv_site) TextView tvSite;
    @BindView(R.id.tv_location) TextView tvLocation;
    @BindView(R.id.tv_bay) TextView tvBay;
    @BindView(R.id.tv_bed) TextView tvBed;
    @BindView(R.id.tv_nhs) TextView tvNhs;
    @BindView(R.id.layout_clinician) LinearLayout layoutClinician;
    @BindView(R.id.layout_location) LinearLayout layoutLocation;
    @BindView(R.id.layout_nhs) LinearLayout layoutNhs;

    public static final String PATIENT_FAMILY_NAME = "PATIENT_FAMILY_NAME";
    public static final String PATIENT_GIVEN_NAME = "PATIENT_GIVEN_NAME";
    public static final String CLINICIAN = "CLINICIAN";
    public static final String SITE = "SITE";
    public static final String LOCATION = "LOCATION";
    public static final String BAY = "BAY";
    public static final String BED = "BED";
    public static final String NHS = "NHS";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        populateData();
    }

    private void populateData() {
        Bundle bundle = null;
        if (getArguments() != null) {
            bundle = getArguments();
        }

        if (bundle == null) return;

        tvPatientLastname.setText(bundle.getString(PATIENT_FAMILY_NAME));
        tvPatientFirstname.setText(bundle.getString(PATIENT_GIVEN_NAME));

        // show data if available
        if (bundle.getString(CLINICIAN) != null) {
            displayClinician(true);
            tvClinician.setText(bundle.getString(CLINICIAN));
        } else {
            displayClinician(false);
        }

        if (bundle.getString(SITE) != null || bundle.getString(LOCATION) != null
                || bundle.getString(BED) != null || bundle.getString(BAY) != null) {
            displayLocation(true);
        } else {
            displayLocation(false);
        }

        // show data if available
        if (bundle.getString(SITE) != null)
            tvSite.setText(bundle.getString(SITE));

        // show data if available
        if (bundle.getString(LOCATION) != null)
            tvLocation.setText(bundle.getString(LOCATION));

        /* Show data if available
           Bay seems to return bed data and vice versa*/
        if (bundle.getString(BED) != null)
            tvBay.setText(bundle.getString(BED));

        /* Show data if available
           Bay seems to return bed data and vice versa*/
        if (bundle.getString(BAY) != null) {
            tvBed.setText(bundle.getString(BAY));
        }


        // show data if available
        if (bundle.getString(NHS) != null) {
            displayNhsNumber(true);
            tvNhs.setText(bundle.getString(NHS));
        } else {
            displayNhsNumber(false);
        }
    }

    private void displayClinician(boolean status) {
        layoutClinician.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    private void displayLocation(boolean status) {
        layoutLocation.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    private void displayNhsNumber(boolean status) {
        layoutNhs.setVisibility(status ? View.VISIBLE : View.GONE);
    }
}
