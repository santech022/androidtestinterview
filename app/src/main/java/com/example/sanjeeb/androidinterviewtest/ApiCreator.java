package com.example.sanjeeb.androidinterviewtest;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.schedulers.Schedulers;

/**
 * This class creates a singleton ApiCreator instance for making retrofit api calls
 * Interceptors are added for headers and logging api calls
 *
 */
public class ApiCreator {

    // singleton instance
    private static ApiCreator instance;

    private Map<String, Object> apiServicesMap;
    private OkHttpClient client;
    private Converter.Factory converterFactory;
    private Gson gson;
    private Retrofit retrofit;

    // private constructor for singleton
    private ApiCreator() {
        apiServicesMap = new HashMap<>();
        client = buildOkHttpClient(getInterceptors());
        gson = createGson();
        converterFactory = createGsonConverterFactory();
    }

    public static ApiCreator instance() {

        if (instance != null) return instance;

        synchronized (ApiCreator.class) {
            if (instance != null) return instance;
            instance = new ApiCreator();
            return instance;
        }
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public <S> S create(Class<S> serviceClass) {
        return create(serviceClass, Config.API_BASE_URL);
    }

    public <S> S create(Class<S> serviceClass, String baseUrl) {
        String serviceMapKey = serviceClass.getName() + baseUrl;

        if (apiServicesMap.get(serviceMapKey) != null) {
            //noinspection unchecked
            return (S) apiServicesMap.get(serviceMapKey);
        }

        retrofit = buildRetrofit(baseUrl);
        S service = retrofit.create(serviceClass);
        apiServicesMap.put(serviceMapKey, service);

        return service;
    }

    public Gson getGson() {
        return gson;
    }

    private Retrofit buildRetrofit(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory)
                .build();
    }

    private Converter.Factory createGsonConverterFactory() {
        return GsonConverterFactory.create(gson);
    }

    private Gson createGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .excludeFieldsWithoutExposeAnnotation()
                .serializeNulls()
                .create();
    }

    private OkHttpClient buildOkHttpClient(List<Interceptor> interceptors) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        for (Interceptor interceptor : interceptors) {
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }

    private List<Interceptor> getInterceptors() {
        List<Interceptor> interceptors = new ArrayList<>();
        // add header interceptor
        interceptors.add(getHeaderInterceptor());

        // add logging interceptor
        if (Config.DEBUG) {
            interceptors.add(getLoggingInterceptor());
        }

        return interceptors;
    }

    private Interceptor getHeaderInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();

                Map<String, String> headers = getHeadersAfterAnnotatedSkip(chain.request().headers());
                for (Map.Entry<String, String> header : headers.entrySet()) {
                    builder.addHeader(header.getKey(), header.getValue());
                }

                return chain.proceed(builder.build());
            }
        };
    }

    private Map<String, String> getHeadersAfterAnnotatedSkip(Headers annotatedHeaders) {
        Map<String, String> configHeaders = new HashMap<>(Config.API_HEADERS);

        for (String headerName : annotatedHeaders.names()) {
            configHeaders.remove(headerName);
        }

        return configHeaders;
    }

    private Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return interceptor;
    }

}
