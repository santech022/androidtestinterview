package com.example.sanjeeb.androidinterviewtest.module.patient.view;

import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;

/**
 * Interface for the launcher activity View
 * */

public interface ViewInterface {

    void displayPatientDetailsSuccess(PatientDetailsResponse response);
    void displayPatientDetailsError(String message);
    void showProgress();
    void hideProgress();
    void showToast(String s);

}
