package com.example.sanjeeb.androidinterviewtest;

import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;

/**
 *  Api class where all the api calls made are listed
 *  Each api calls returns an Observable which are observed by the concerned entities
 * */

public interface NetworkInterfaceApi {

    /**
     * As the body of the api response is null we are using "Void"
     * Response object is used as we are interested in retrieving headers which contains the "access_token"
     * */
    @FormUrlEncoded
    @POST
    Observable<Response<Void>> getAuthDetails(
            @Url String url,
            @Header("Content-Type") String contentType,
            @Field("EmailAddress") String emailAddress,
            @Field("Password") String password,
            @Field("client_id") String clientId,
            @Field("redirect_uri") String redirectUri,
            @Field("response_type") String responseType
            );

    @POST("groups/3061/patients")
    Observable<PatientDetailsResponse> getPatientDetails(
            @Header("Content-Type") String contentType,
            @Header("Accept") String accept,
            @Header("Authorization") String authorization,
            @Body AuthRequestBody authRequestBody
            );
}
