package com.example.sanjeeb.androidinterviewtest.module.patient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model classes for fetching patient details.
 * */

public class PatientDetailsResponse {

    @SerializedName("Metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("Data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("Messages")
    @Expose
    private List<Message> messages = null;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public PatientDetailsResponse withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public PatientDetailsResponse withData(List<Datum> data) {
        this.data = data;
        return this;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public PatientDetailsResponse withMessages(List<Message> messages) {
        this.messages = messages;
        return this;
    }

    public class PatientIdentifiers {

        @SerializedName("PrimaryIdentifier")
        @Expose
        private PrimaryIdentifier primaryIdentifier;
        @SerializedName("AlternativeIdentifiers")
        @Expose
        private List<Object> alternativeIdentifiers = null;

        public PrimaryIdentifier getPrimaryIdentifier() {
            return primaryIdentifier;
        }

        public void setPrimaryIdentifier(PrimaryIdentifier primaryIdentifier) {
            this.primaryIdentifier = primaryIdentifier;
        }

        public PatientIdentifiers withPrimaryIdentifier(PrimaryIdentifier primaryIdentifier) {
            this.primaryIdentifier = primaryIdentifier;
            return this;
        }

        public List<Object> getAlternativeIdentifiers() {
            return alternativeIdentifiers;
        }

        public void setAlternativeIdentifiers(List<Object> alternativeIdentifiers) {
            this.alternativeIdentifiers = alternativeIdentifiers;
        }

        public PatientIdentifiers withAlternativeIdentifiers(List<Object> alternativeIdentifiers) {
            this.alternativeIdentifiers = alternativeIdentifiers;
            return this;
        }

    }

    public class PrimaryIdentifier {

        @SerializedName("Label")
        @Expose
        private String label;
        @SerializedName("Value")
        @Expose
        private String value;
        @SerializedName("Status")
        @Expose
        private Object status;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public PrimaryIdentifier withLabel(String label) {
            this.label = label;
            return this;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public PrimaryIdentifier withValue(String value) {
            this.value = value;
            return this;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public PrimaryIdentifier withStatus(Object status) {
            this.status = status;
            return this;
        }

    }

    public class Pagination {

        @SerializedName("TotalItems")
        @Expose
        private Integer totalItems;
        @SerializedName("RetrievedItems")
        @Expose
        private Integer retrievedItems;
        @SerializedName("ThereAreMore")
        @Expose
        private Boolean thereAreMore;

        public Integer getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
        }

        public Pagination withTotalItems(Integer totalItems) {
            this.totalItems = totalItems;
            return this;
        }

        public Integer getRetrievedItems() {
            return retrievedItems;
        }

        public void setRetrievedItems(Integer retrievedItems) {
            this.retrievedItems = retrievedItems;
        }

        public Pagination withRetrievedItems(Integer retrievedItems) {
            this.retrievedItems = retrievedItems;
            return this;
        }

        public Boolean getThereAreMore() {
            return thereAreMore;
        }

        public void setThereAreMore(Boolean thereAreMore) {
            this.thereAreMore = thereAreMore;
        }

        public Pagination withThereAreMore(Boolean thereAreMore) {
            this.thereAreMore = thereAreMore;
            return this;
        }

    }

    public class Metadata {

        @SerializedName("Pagination")
        @Expose
        private Pagination pagination;

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public Metadata withPagination(Pagination pagination) {
            this.pagination = pagination;
            return this;
        }

    }

    public class Message {

        @SerializedName("Code")
        @Expose
        private Integer code;
        @SerializedName("Message")
        @Expose
        private String message;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public Message withCode(Integer code) {
            this.code = code;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Message withMessage(String message) {
            this.message = message;
            return this;
        }

    }

    public class Datum {

        @SerializedName("PatientIdentifiers")
        @Expose
        private PatientIdentifiers patientIdentifiers;
        @SerializedName("AgeDescription")
        @Expose
        private String ageDescription;
        @SerializedName("PatientTitle")
        @Expose
        private Object patientTitle;
        @SerializedName("PatientGivenName")
        @Expose
        private String patientGivenName;
        @SerializedName("PatientFamilyName")
        @Expose
        private String patientFamilyName;
        @SerializedName("PatientGender")
        @Expose
        private String patientGender;
        @SerializedName("PatientDateOfBirth")
        @Expose
        private String patientDateOfBirth;
        @SerializedName("Address")
        @Expose
        private Object address;
        @SerializedName("TelephoneNumber")
        @Expose
        private String telephoneNumber;
        @SerializedName("Clinician")
        @Expose
        private Clinician clinician;
        @SerializedName("SiteName")
        @Expose
        private String siteName;
        @SerializedName("AreaName")
        @Expose
        private String areaName;
        @SerializedName("Bay")
        @Expose
        private String bay;
        @SerializedName("BaySort")
        @Expose
        private String baySort;
        @SerializedName("Bed")
        @Expose
        private String bed;
        @SerializedName("BedSort")
        @Expose
        private String bedSort;
        @SerializedName("EstimatedDischargeDate")
        @Expose
        private Object estimatedDischargeDate;
        @SerializedName("AdmissionDate")
        @Expose
        private Object admissionDate;
        @SerializedName("DateOfDeath")
        @Expose
        private Object dateOfDeath;
        @SerializedName("PatientExternalIdentifier")
        @Expose
        private String patientExternalIdentifier;
        @SerializedName("GroupListExternalIdentifiers")
        @Expose
        private List<String> groupListExternalIdentifiers = null;
        @SerializedName("NetworkAccessGroupExternalIdentifier")
        @Expose
        private String networkAccessGroupExternalIdentifier;
        @SerializedName("PatientNotFound")
        @Expose
        private Boolean patientNotFound;
        @SerializedName("OrganisationIdentifier")
        @Expose
        private Object organisationIdentifier;
        @SerializedName("IsInGroup")
        @Expose
        private Boolean isInGroup;

        public PatientIdentifiers getPatientIdentifiers() {
            return patientIdentifiers;
        }

        public void setPatientIdentifiers(PatientIdentifiers patientIdentifiers) {
            this.patientIdentifiers = patientIdentifiers;
        }

        public Datum withPatientIdentifiers(PatientIdentifiers patientIdentifiers) {
            this.patientIdentifiers = patientIdentifiers;
            return this;
        }

        public String getAgeDescription() {
            return ageDescription;
        }

        public void setAgeDescription(String ageDescription) {
            this.ageDescription = ageDescription;
        }

        public Datum withAgeDescription(String ageDescription) {
            this.ageDescription = ageDescription;
            return this;
        }

        public Object getPatientTitle() {
            return patientTitle;
        }

        public void setPatientTitle(Object patientTitle) {
            this.patientTitle = patientTitle;
        }

        public Datum withPatientTitle(Object patientTitle) {
            this.patientTitle = patientTitle;
            return this;
        }

        public String getPatientGivenName() {
            return patientGivenName;
        }

        public void setPatientGivenName(String patientGivenName) {
            this.patientGivenName = patientGivenName;
        }

        public Datum withPatientGivenName(String patientGivenName) {
            this.patientGivenName = patientGivenName;
            return this;
        }

        public String getPatientFamilyName() {
            return patientFamilyName;
        }

        public void setPatientFamilyName(String patientFamilyName) {
            this.patientFamilyName = patientFamilyName;
        }

        public Datum withPatientFamilyName(String patientFamilyName) {
            this.patientFamilyName = patientFamilyName;
            return this;
        }

        public String getPatientGender() {
            return patientGender;
        }

        public void setPatientGender(String patientGender) {
            this.patientGender = patientGender;
        }

        public Datum withPatientGender(String patientGender) {
            this.patientGender = patientGender;
            return this;
        }

        public String getPatientDateOfBirth() {
            return patientDateOfBirth;
        }

        public void setPatientDateOfBirth(String patientDateOfBirth) {
            this.patientDateOfBirth = patientDateOfBirth;
        }

        public Datum withPatientDateOfBirth(String patientDateOfBirth) {
            this.patientDateOfBirth = patientDateOfBirth;
            return this;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public Datum withAddress(Object address) {
            this.address = address;
            return this;
        }

        public String getTelephoneNumber() {
            return telephoneNumber;
        }

        public void setTelephoneNumber(String telephoneNumber) {
            this.telephoneNumber = telephoneNumber;
        }

        public Datum withTelephoneNumber(String telephoneNumber) {
            this.telephoneNumber = telephoneNumber;
            return this;
        }

        public Clinician getClinician() {
            return clinician;
        }

        public void setClinician(Clinician clinician) {
            this.clinician = clinician;
        }

        public Datum withClinician(Clinician clinician) {
            this.clinician = clinician;
            return this;
        }

        public String getSiteName() {
            return siteName;
        }

        public void setSiteName(String siteName) {
            this.siteName = siteName;
        }

        public Datum withSiteName(String siteName) {
            this.siteName = siteName;
            return this;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public Datum withAreaName(String areaName) {
            this.areaName = areaName;
            return this;
        }

        public String getBay() {
            return bay;
        }

        public void setBay(String bay) {
            this.bay = bay;
        }

        public Datum withBay(String bay) {
            this.bay = bay;
            return this;
        }

        public String getBaySort() {
            return baySort;
        }

        public void setBaySort(String baySort) {
            this.baySort = baySort;
        }

        public Datum withBaySort(String baySort) {
            this.baySort = baySort;
            return this;
        }

        public String getBed() {
            return bed;
        }

        public void setBed(String bed) {
            this.bed = bed;
        }

        public Datum withBed(String bed) {
            this.bed = bed;
            return this;
        }

        public String getBedSort() {
            return bedSort;
        }

        public void setBedSort(String bedSort) {
            this.bedSort = bedSort;
        }

        public Datum withBedSort(String bedSort) {
            this.bedSort = bedSort;
            return this;
        }

        public Object getEstimatedDischargeDate() {
            return estimatedDischargeDate;
        }

        public void setEstimatedDischargeDate(Object estimatedDischargeDate) {
            this.estimatedDischargeDate = estimatedDischargeDate;
        }

        public Datum withEstimatedDischargeDate(Object estimatedDischargeDate) {
            this.estimatedDischargeDate = estimatedDischargeDate;
            return this;
        }

        public Object getAdmissionDate() {
            return admissionDate;
        }

        public void setAdmissionDate(Object admissionDate) {
            this.admissionDate = admissionDate;
        }

        public Datum withAdmissionDate(Object admissionDate) {
            this.admissionDate = admissionDate;
            return this;
        }

        public Object getDateOfDeath() {
            return dateOfDeath;
        }

        public void setDateOfDeath(Object dateOfDeath) {
            this.dateOfDeath = dateOfDeath;
        }

        public Datum withDateOfDeath(Object dateOfDeath) {
            this.dateOfDeath = dateOfDeath;
            return this;
        }

        public String getPatientExternalIdentifier() {
            return patientExternalIdentifier;
        }

        public void setPatientExternalIdentifier(String patientExternalIdentifier) {
            this.patientExternalIdentifier = patientExternalIdentifier;
        }

        public Datum withPatientExternalIdentifier(String patientExternalIdentifier) {
            this.patientExternalIdentifier = patientExternalIdentifier;
            return this;
        }

        public List<String> getGroupListExternalIdentifiers() {
            return groupListExternalIdentifiers;
        }

        public void setGroupListExternalIdentifiers(List<String> groupListExternalIdentifiers) {
            this.groupListExternalIdentifiers = groupListExternalIdentifiers;
        }

        public Datum withGroupListExternalIdentifiers(List<String> groupListExternalIdentifiers) {
            this.groupListExternalIdentifiers = groupListExternalIdentifiers;
            return this;
        }

        public String getNetworkAccessGroupExternalIdentifier() {
            return networkAccessGroupExternalIdentifier;
        }

        public void setNetworkAccessGroupExternalIdentifier(String networkAccessGroupExternalIdentifier) {
            this.networkAccessGroupExternalIdentifier = networkAccessGroupExternalIdentifier;
        }

        public Datum withNetworkAccessGroupExternalIdentifier(String networkAccessGroupExternalIdentifier) {
            this.networkAccessGroupExternalIdentifier = networkAccessGroupExternalIdentifier;
            return this;
        }

        public Boolean getPatientNotFound() {
            return patientNotFound;
        }

        public void setPatientNotFound(Boolean patientNotFound) {
            this.patientNotFound = patientNotFound;
        }

        public Datum withPatientNotFound(Boolean patientNotFound) {
            this.patientNotFound = patientNotFound;
            return this;
        }

        public Object getOrganisationIdentifier() {
            return organisationIdentifier;
        }

        public void setOrganisationIdentifier(Object organisationIdentifier) {
            this.organisationIdentifier = organisationIdentifier;
        }

        public Datum withOrganisationIdentifier(Object organisationIdentifier) {
            this.organisationIdentifier = organisationIdentifier;
            return this;
        }

        public Boolean getIsInGroup() {
            return isInGroup;
        }

        public void setIsInGroup(Boolean isInGroup) {
            this.isInGroup = isInGroup;
        }

        public Datum withIsInGroup(Boolean isInGroup) {
            this.isInGroup = isInGroup;
            return this;
        }

    }

    public class Clinician {

        @SerializedName("FullName")
        @Expose
        private String fullName;
        @SerializedName("FirstName")
        @Expose
        private String firstName;
        @SerializedName("LastName")
        @Expose
        private String lastName;
        @SerializedName("Title")
        @Expose
        private Object title;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public Clinician withFullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Clinician withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public Clinician withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Object getTitle() {
            return title;
        }

        public void setTitle(Object title) {
            this.title = title;
        }

        public Clinician withTitle(Object title) {
            this.title = title;
            return this;
        }

    }
}
