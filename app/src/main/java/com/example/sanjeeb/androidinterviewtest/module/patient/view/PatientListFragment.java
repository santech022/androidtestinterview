package com.example.sanjeeb.androidinterviewtest.module.patient.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sanjeeb.androidinterviewtest.R;
import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;
import com.example.sanjeeb.androidinterviewtest.module.patient.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

/**
 * This class (fragment) is used to show the patient names
 * */

public class PatientListFragment extends Fragment implements ViewInterface, PatientDataAdapter.ItemClickListener {

    @BindView(R.id.rv_patients) RecyclerView rvPatients;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private Presenter presenter;
    private Subscription subscription;
    private PatientDataAdapter patientListAdapter;
    private List<PatientDetailsResponse.Datum> patientList = new ArrayList<>();
    private OnPatientSelectedListener onPatientSelectedListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onPatientSelectedListener = (OnPatientSelectedListener) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclerAdapter();

        presenter = new Presenter(this);
        subscription = presenter.getAuthObservable();
        showProgress();
    }


    @Override
    public void displayPatientDetailsSuccess(PatientDetailsResponse response) {
        if (response == null) return;
        hideProgress();
        patientList.addAll(response.getData());
        patientListAdapter.notifyDataSetChanged();
    }

    @Override
    public void displayPatientDetailsError(String message) {
        showToast(message);
    }

    @Override
    public void showToast(String s) {
        Toast.makeText(getActivity(),s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(PatientDetailsResponse.Datum item) {
        onPatientSelectedListener.onPatientSelected(item);

    }

    @Override
    public void onPause() {
        super.onPause();
        subscription.unsubscribe();
    }

    private void setupRecyclerAdapter() {
        patientListAdapter = new PatientDataAdapter(getActivity(), patientList, this);
        rvPatients.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPatients.setAdapter(patientListAdapter);
    }

    public interface OnPatientSelectedListener {
        void onPatientSelected(PatientDetailsResponse.Datum item);
    }
}
