package com.example.sanjeeb.androidinterviewtest.module.patient.presenter;

import rx.Observable;
import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import com.example.sanjeeb.androidinterviewtest.ApiCreator;
import com.example.sanjeeb.androidinterviewtest.Config;
import com.example.sanjeeb.androidinterviewtest.AuthRequestBody;
import com.example.sanjeeb.androidinterviewtest.NetworkInterfaceApi;
import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;
import com.example.sanjeeb.androidinterviewtest.module.patient.model.TokenData;
import com.example.sanjeeb.androidinterviewtest.module.patient.view.ViewInterface;
import com.example.sanjeeb.androidinterviewtest.module.util.UtilClass;

/**
 * This class is responsible for making the api calls for
 *      a) authentication
 *      b) getting patient details
 *
 * The access_token is extracted from the authentication response and added as a header in
 * the subsequent api call for fetching patient details
 * */

public class Presenter {

    private ViewInterface viewInterface;
    private NetworkInterfaceApi api;

    public Presenter(ViewInterface viewInterface) {
    this.viewInterface = viewInterface;
    api = ApiCreator.instance().create(NetworkInterfaceApi.class);
    }

    public Subscription getAuthObservable() {

        return api.getAuthDetails( Config.API_AUTH_URL,
                        Config.CONTENT_TYPE,
                        Config.EMAIL_ADDRESS,
                        Config.PASSWORD,
                        Config.CLIENT_ID,
                        Config.REDIRECT_URL,
                        Config.RESPONSE_TYPE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Func1<Response<Void>, Observable<PatientDetailsResponse>>() {
                    @Override
                    public Observable<PatientDetailsResponse> call(Response<Void> voidResponse) {
                        TokenData tokenData = UtilClass.stringToJson(
                                voidResponse.headers().get(Config.ACCESS_TOKEN));
                        return api.getPatientDetails(
                                Config.CONTENT_TYPE,
                                Config.ACCEPT,
                                Config.BEARER + tokenData.getToken(),
                                new AuthRequestBody().getAuthRequestBody());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<PatientDetailsResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.displayPatientDetailsError(e.getMessage());
                    }

                    @Override
                    public void onNext(PatientDetailsResponse patientDetailsResponse) {
                        viewInterface.displayPatientDetailsSuccess(patientDetailsResponse);
                    }
                });

    }



}
