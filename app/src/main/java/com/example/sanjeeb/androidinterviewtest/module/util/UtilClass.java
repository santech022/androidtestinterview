package com.example.sanjeeb.androidinterviewtest.module.util;

import com.example.sanjeeb.androidinterviewtest.module.patient.model.TokenData;
import com.google.gson.Gson;

public class UtilClass {

    public static TokenData stringToJson(String string) {

        Gson gson = new Gson();
        return gson.fromJson(string, TokenData.class);
    }
}
