package com.example.sanjeeb.androidinterviewtest.module.patient.view;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.sanjeeb.androidinterviewtest.R;
import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;
import com.example.sanjeeb.androidinterviewtest.module.patientdetails.PatientDetailsActivity;
import com.example.sanjeeb.androidinterviewtest.module.patientdetails.PatientDetailsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Launcher activity hosts PatientListFragment that shows the list of patients.
 *
 * Presenter object is used to get a subscription to initiate the api calls.
 * The subscription is unsubscribed in the onPause callback method.
 *
 * Mobile devices - On click of each patient, the "PatientDetailsActivity" is shown.
 * Tablets - On click of each patient, PatientDetailsFragment fragments are shown with patient details.
 * */

public class MainActivity extends AppCompatActivity implements PatientListFragment.OnPatientSelectedListener {

    @BindView(R.id.container) FrameLayout container;

    private boolean isTwoPane;
    public static final String PATIENTS_LIST = "Patients List";
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) getSupportActionBar().setTitle(PATIENTS_LIST);

        isTwoPane = findViewById(R.id.detailsContainer) != null;

        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, new PatientListFragment())
                    .commit();
        }
    }

    @Override
    public void onPatientSelected(PatientDetailsResponse.Datum item) {

        Bundle bundle = new Bundle();
        bundle.putString(PatientDetailsFragment.PATIENT_FAMILY_NAME, item.getPatientFamilyName());
        bundle.putString(PatientDetailsFragment.PATIENT_GIVEN_NAME, item.getPatientGivenName());

        //show data if available
        if (item.getClinician() != null)
            bundle.putString(PatientDetailsFragment.CLINICIAN, item.getClinician().getFullName());

        //show data if available
        if (item.getSiteName() != null)
            bundle.putString(PatientDetailsFragment.SITE, item.getSiteName());

        //show data if available
        if (item.getAreaName() != null)
            bundle.putString(PatientDetailsFragment.LOCATION, item.getAreaName());

        //show data if available
        if (item.getBay() != null)
            bundle.putString(PatientDetailsFragment.BAY, item.getBay());

        //show data if available
        if (item.getBed() != null)
            bundle.putString(PatientDetailsFragment.BED, item.getBed());

        //show data if available
        if (item.getPatientExternalIdentifier() != null)
            bundle.putString(PatientDetailsFragment.NHS, item.getPatientExternalIdentifier());

        PatientDetailsFragment frag = new PatientDetailsFragment();
        if (isTwoPane) {
            frag.setArguments(bundle);

            fragmentManager.beginTransaction()
                    .replace(R.id.detailsContainer, frag)
                    .commit();

        } else {
            Intent intent = new Intent(this, PatientDetailsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

}
