package com.example.sanjeeb.androidinterviewtest;

import java.util.HashMap;
import java.util.Map;

/**
 * File to maintain app level configurations
 */

public class Config {

    //--------------------------------------------------------------------------------
    // App generic configurations
    //--------------------------------------------------------------------------------
    public static final boolean DEBUG = BuildConfig.DEBUG;

    //--------------------------------------------------------------------------------
    // API related constants/configurations - used in ApiModule
    //--------------------------------------------------------------------------------
    public static String API_AUTH_URL = "https://appdevauth.doccom.me/Home/Login";
    public static String API_BASE_URL = "https://appdevapi.doccom.me/";

    public static String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public static String EMAIL_ADDRESS = "careflowinterviewcandidate@gmail.com";
    public static String PASSWORD = "Car3F!0wT3st";
    public static String CLIENT_ID = "DocComMobile";
    public static String REDIRECT_URL = "https://www.careflowconnect.com";
    public static String RESPONSE_TYPE = "token";
    public static String ACCEPT = "application/json;version=10";
    public static String BEARER = "Bearer ";
    public static String ACCESS_TOKEN = "access_token";

    // Common http headers required to be added by retrofit
    public static final Map<String, String> API_HEADERS = new HashMap<String, String>() {{
        // put("User-Agent", "TLConnect-Android-App");
        put("Content-Type", "application/json");
        put("Connection", "close");
    }};


}
