package com.example.sanjeeb.androidinterviewtest.module.patient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This class is used for extracting access_token returned as header in authorization api response
 * */

public class TokenData {


    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Provider")
    @Expose
    private String provider;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

}
