package com.example.sanjeeb.androidinterviewtest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

/**
 * Class used for sending required data while making request for fetching patient details
 * */

public class AuthRequestBody {

    @SerializedName("ItemsPerPage")
    @Expose
    private Integer itemsPerPage;
    @SerializedName("Include")
    @Expose
    private List<String> include = null;
    @SerializedName("Page")
    @Expose
    private Integer page;

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }


    public List<String> getInclude() {
        return include;
    }

    public void setInclude(List<String> include) {
        this.include = include;
    }


    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public AuthRequestBody getAuthRequestBody() {

        String[] includeArray = {"HandoverNotes"};

        AuthRequestBody authRequestBody = new AuthRequestBody();
        authRequestBody.setItemsPerPage(50);
        authRequestBody.setInclude(Arrays.asList(includeArray));
        authRequestBody.setPage(0);

        return authRequestBody;
    }

}
