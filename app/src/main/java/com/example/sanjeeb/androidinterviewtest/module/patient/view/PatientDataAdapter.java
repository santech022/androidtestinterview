package com.example.sanjeeb.androidinterviewtest.module.patient.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sanjeeb.androidinterviewtest.R;
import com.example.sanjeeb.androidinterviewtest.module.patient.model.PatientDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *  RecyclerView adapter for showing the patients.
 *  On click of each patient a new activity is shown with patient details.
 * */

public class PatientDataAdapter extends RecyclerView.Adapter<PatientDataAdapter.ViewHolder> {

    private Context context;
    private List<PatientDetailsResponse.Datum> patientList;
    private ItemClickListener itemClickListener;


    public PatientDataAdapter(Context context, List<PatientDetailsResponse.Datum> patientList,
                              ItemClickListener itemClickListener) {
        this.context = context;
        this.patientList = patientList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(
                R.layout.patient_list_item_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return patientList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_patient_lastname) TextView tvPatientName;
        @BindView(R.id.layout_patient) LinearLayout layoutPatient;

        private int position;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(int position) {
            this.position = position;
            PatientDetailsResponse.Datum item = patientList.get(position);
            tvPatientName.setText(item.getPatientGivenName());
        }

        @OnClick(R.id.layout_patient)
        void onClickPatient() {
            itemClickListener.onItemClick(patientList.get(getAdapterPosition()));
        }

    }

    /**
     * Interface created for capturing click events on each patient
     * */
    public interface ItemClickListener {

        void onItemClick(PatientDetailsResponse.Datum item);
    }
}
