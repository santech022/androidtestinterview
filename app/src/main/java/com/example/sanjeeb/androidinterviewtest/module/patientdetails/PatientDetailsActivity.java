package com.example.sanjeeb.androidinterviewtest.module.patientdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import com.example.sanjeeb.androidinterviewtest.R;

import butterknife.ButterKnife;

/**
 * This activity shows the details of each patient when clicked when used with Mobile devices
 * */

public class PatientDetailsActivity extends AppCompatActivity {

    public static final String PATIENT_DETAILS = "Patient Details";
    public static final String FRAGMENT_TAG = "parentDetailsFragmentTag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(PATIENT_DETAILS);

        Intent intent = null;
        if (getIntent() != null) {
            intent = getIntent();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        PatientDetailsFragment fragment = new PatientDetailsFragment();

        if (intent.getExtras() != null) {
            fragment.setArguments(intent.getExtras());
        }

        fragmentManager.beginTransaction()
                .add(R.id.patient_details_container, fragment, FRAGMENT_TAG)
                .commit();
    }
}
